#include <iostream>
#include <string>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>

class obd_module {

private:
//======================================================================================//
	int serial_write(const char* data, int bytes_to_send)
	{

		tcflush(serialPort, TCIOFLUSH);
		std::cout << "data to write " << data << std::endl;
		c = write(serialPort,  data, (bytes_to_send-1));
		std::cout << "NOB written:" << c << std::endl;
		serial_read(recv_char, 1);
	}
//======================================================================================//
	int serial_read(char ch, int bytes_to_read)
	{
		recv = "";
		term = ">";
		while(c >= 0)
		{
			c = read(serialPort, &ch,1);
			printf("Char is %c\n", ch);
			recv = recv + ch;
			size_t found = recv.find(term);
			if(found != std::string::npos)
			{
				return 0;
			}
		}
	}
//=======================================================================================//
//=======================================================================================//
public:

	int serialPort;
	const char reset_command[6] = {'A','T','Z','\r','\n'};
	const char set_protocol_command[8] = {'A','T','S','P','0','\r','\n'};
	const char speed_command[7] = {'0','1','0','d','\r','\n'};
	struct termios portOptions;
	char recv_char;
	std::string recv;
	std::string term;
	int c;
//=======================================================================================//
	void connect(const char* portName)
	{
/*Open the serial port with open() system call
O_RDWR:   Open the file in read/write mode
O_NDELAY: If possible, open the file in nonblocking mode
O_NOCTTY: If the file is of a serial device, terminal becomes the controlling process */

		serialPort = open(portName, O_RDWR | O_NOCTTY | O_NDELAY);

		//Get the present attribute flags of the terminal
		tcgetattr(serialPort, &portOptions);

		//Flush the terminal IO before sending anything
		tcflush(serialPort, TCIOFLUSH);

		//Set terminal input/output baudrates(Device defaults at 9600)
		cfsetispeed(&portOptions, B9600);
		cfsetospeed(&portOptions, B9600);

		//Ignore MODEM control lines
		portOptions.c_cflag |= CLOCAL;
		//Enable receiver
		portOptions.c_cflag |= CREAD;
		//Disable data bits
		portOptions.c_cflag &= ~CSIZE;
		//Set data bits to 8
		portOptions.c_cflag |= CS8;
		//Disable parity
		portOptions.c_cflag &= ~PARENB;
		//Disable stop bits(defaults to 1)
		portOptions.c_cflag &= ~CSTOPB;

		//Set attribute flags of the serial ports
		tcsetattr(serialPort, TCSANOW, &portOptions);

		//Set reading to blocking(not the recommended way to do it
		fcntl(serialPort, F_SETFL, 0);

/*Startup Procedure:
At power ON, sending ATZ brings the device to the default state. 
ATSP0 lets the interpreter to automatically choose an appropriate communication protocol with the
vehicle.
Reason for sending the speed command here is to do away with the lag the interpreter faces when it talks
to the vehicle for the first time after power on or after sending ATZ.
*/
		if((serial_write(reset_command, sizeof(reset_command))) == 0)
		{
			if((serial_write(set_protocol_command, sizeof(set_protocol_command))) == 0)
			{
				serial_write(speed_command, sizeof(speed_command));
			}
		}
	}
//=========================================================================================//
	void disconnect()
	{
		printf("disconnecting...\n");
		close(serialPort);
	}
//=========================================================================================//
	int getspeed()
/*If successful, the recieve string would have "41 0D xx" (xx: speed in km/hr)
  find the speed index using str.find()
  copy it in a new string after skipping 6 characters
  new string would have the speed in string format */
	{
		for(int i = 0; i<2; i++)
		{
			serial_write("010d\r\n", sizeof(speed_command));
			int index = recv.find("41 0D ");
			std::string speed = recv.substr(index + 6);
			std::cout << "Speed is " << speed << std::endl;
			//std::string speed1 = "00\n\n";
			int intspeed = std::stoi(speed);
			return intspeed;
			//std::cout << speed1 << "----->" << n << std::endl;
		}

	}
//=========================================================================================//
};
