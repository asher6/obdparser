#include "obd_api.h"

static const char* portName = "/dev/ttyUSB0";

int main()
{
	int speed;
	obd_module o1;
	o1.connect(portName);
	speed = o1.getspeed();
	printf("%d\n", speed);
	return 0;
}
